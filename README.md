# Basic systemd service runs python window
A systemd service to run a python script. Under a python virtual environment also raising a window and self restarts on failure and once a day. 

## Tutorial
- Check sequentially the files `example_[0,1,2,...].service` to get the hang of it
1. Install the service putting the file directly or better symbolic link it:
	```
	cd /etc/systemd/service/
	# ln -s path/to/example.service .
	```

2. Each time you modify the example.service file you need to reload and restart:
	```
	# systemctl daemon-reload
	# systemctl start example
	```

3. Then check results (status) with
	```
	$ systemctl --user status example
	```

4. To activate the python virtual environment make sure it is executable by root (`chmod a+x /path/to/venv/bin/activate`), then you can activate it as usual:
	```
	Exec=bash -c "source /path/to/venv/bin/activate && python script.py"
	```

5. To open a window you must pass these environment variables (DISPLAY value may vary) to the service file:
	```
	Environment=DISPLAY=:1 XAUTHORITY=/home/user/.Xauthority
	```
6. To make it restart
	```
	Restart=always
	RuntimeMaxSec=1d
	```


## Useful links
https://www.freedesktop.org/software/systemd/man/systemd.html

https://gist.github.com/ricferr/90583f608f0b0ae9c3cf6833be04ab85
