#!python3
import time, os
import cv2 as cv
print('This prints on the log if it fails')
f = open('log.txt', 'a')
f.write('Python Hello World!\n'+\
        os.getcwd()+'\n'+\
        cv.__version__+'\n'+\
        time.strftime('%c')+'\n'+\
        'Bye!\n')
f.close()
